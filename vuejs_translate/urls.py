from django.urls import path
from django.conf.urls.i18n import i18n_patterns

from .views import JavaScriptCatalogView

app_name = 'vuejs_translate'

urlpatterns = i18n_patterns(
    path('vuejs-translate', include('vuejs_translate.urls')),
)
